﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    public Camera playerCamera;
    public Text playerNameText;
    public Transform pointer;
    public float speed = 3f;
    CharacterController charController;
    [SerializeField] MouseLook mouseLook;

    private void Start() {
        charController = GetComponent<CharacterController>();
        mouseLook = new MouseLook();
        mouseLook.Init(gameObject.transform, pointer);
        playerNameText.text = PlayerPrefs.GetString("Name", string.Empty);
    }

    private void Update() {
        mouseLook.LookRotation(transform, pointer);
        //Vector3 forward = playerCamera.transform.forward;
        //forward *= speed * Input.GetAxis("Vertical");
        //charController.SimpleMove(forward);
        mouseLook.UpdateCursorLock();
    }

    public void MoveForward() {
        Vector3 forward = playerCamera.transform.forward;
        forward *= speed * 20;
        charController.SimpleMove(forward);
    }

}


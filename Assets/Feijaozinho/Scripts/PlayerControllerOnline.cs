﻿using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using System;
using System.Collections;

public class PlayerControllerOnline :  NetworkBehaviour {

    public Camera playerCamera;

    public Transform pointer;
    public float speed = 3f;
    CharacterController charController;
    public bool mouseLookEnabled = true;
    [SerializeField] MouseLook mouseLook;

    void Start() {
        if (!isLocalPlayer)
            return;

        charController = GetComponent<CharacterController>();
        mouseLook = new MouseLook();
        mouseLook.Init(gameObject.transform, pointer);
        playerCamera.enabled = isLocalPlayer;
        playerCamera.GetComponent<AudioListener>().enabled = isLocalPlayer;
        if (NetManager.netSingleton.IsTeacher) {
            mouseLook.SetCursorLock(false);
            mouseLookEnabled = false;
        } else {
            FloorMovement floor = GameObject.Find("ClassRoom").GetComponentInChildren<FloorMovement>();
            //floor.SetOnlinePlayerMovement(this);
            floor.TimeInput.OnGazeSelect += this.MoveForward;
            Debug.Log("Players Position: " + gameObject.transform.position);
            Debug.Log(String.Format("Floor is null? == {0}", floor == null));
            Debug.Log(String.Format("Floor Move == playerMov? == {0}", floor.TimeInput.OnGazeSelect == this.MoveForward));
        }
    }   

    void Update () {
        if (!isLocalPlayer)
            return;
        MouseRotation();
        if (NetManager.netSingleton.IsTeacher)
            MoveTeacher();
    }

    void MoveTeacher() {
        Vector3 forward = playerCamera.transform.forward;
        forward *= speed * Input.GetAxis("Vertical");
        charController.SimpleMove(forward);
        transform.Rotate(0, Input.GetAxis("Horizontal") * speed, 0);
    }

    void MouseRotation() {
        if (mouseLookEnabled)
            mouseLook.LookRotation(gameObject.transform, pointer.transform);

        if (mouseLookEnabled)
            mouseLook.UpdateCursorLock();
    }

    public void MoveForward() {
        if (!isLocalPlayer)
            return;

        Vector3 forward = playerCamera.transform.forward;
        forward *= speed * 20f;
        charController.SimpleMove(forward);
    }

    [Command]
    public void CmdSetColor(bool isSpeaking) {
        Color color;
        if (isSpeaking)
            color = Color.green;
        else
            color = Color.white;

        OnServerSetColor(color);
    }
    
    [Server]
    void OnServerSetColor(Color color) {
        RpcSetColor(color);
    }

    [ClientRpc]
    void RpcSetColor(Color color) {
        GetComponent<Renderer>().material.color = color;
    }
}

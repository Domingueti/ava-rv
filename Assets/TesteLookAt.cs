﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TesteLookAt : NetworkBehaviour {

    public GameObject prefab;

	// Use this for initialization
	void Start () {
        GameObject obj = Instantiate(prefab) as GameObject;
        NetworkServer.Spawn(obj);
	}
}

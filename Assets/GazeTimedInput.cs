﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class GazeTimedInput : MonoBehaviour {

    
    public Action OnGazeSelect;
    public bool disableOnSelect = true;
    public float selectTime = 2f;
    private bool hasSelected = false;
    private float startTime = -1f;
    private Image image; //if it is a button
    private Color enabledColor;
    void Start () {
        image = GetComponent<Image>();
        if (image != null)
            enabledColor = image.color;
    }

    private void Update() {
        if (hasSelected && startTime > 0) {
            if (Time.time - startTime >= selectTime) {
                OnGazeSelect?.Invoke(); //if (OnGazeSelect != null) OnGazeSelect();
                if (disableOnSelect) {
                    hasSelected = false;
                    startTime = -1f;
                } else {
                    startTime = Time.time;
                }
                
                if (image != null)
                    image.color = enabledColor;
            }
                
        }
    }
    

    public void OnPointerEnter() {
        if (image != null)
            image.color = Color.gray;

        hasSelected = true;
        startTime = Time.time;
    }

    public void OnPointerExit() { 
        if (image != null)
            image.color = enabledColor;

        hasSelected = false;
        startTime = -1f;
    }
}

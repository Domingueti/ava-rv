﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FloorMovement : MonoBehaviour {

    private GazeTimedInput input;
    private GameObject player;

    public GazeTimedInput TimeInput { get { return this.input; } }

    private void Start() {
        input = GetComponent<GazeTimedInput>();
        if (SceneManager.GetActiveScene().name == "MainLobby") {
            player = GameObject.FindGameObjectWithTag("Player");
            if (input != null) {
                input.OnGazeSelect += player.GetComponent<PlayerMovement>().MoveForward;
            }
        }
    }

    public void SetPlayerMovement(PlayerMovement player) {
        input = GetComponent<GazeTimedInput>();
        input.OnGazeSelect += player.MoveForward;
    }

    public void SetOnlinePlayerMovement(PlayerControllerOnline player) {
        input.OnGazeSelect += player.MoveForward;
    }

}

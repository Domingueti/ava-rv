﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSceneLoaded : MonoBehaviour {

    // Use this for initialization
    void Start() {
        GameObject room = GameObject.Find("Room");
        if (room != null) {
            FloorMovement input = room.GetComponentInChildren<FloorMovement>();
            input.SetPlayerMovement(GetComponent<PlayerMovement>());
        }
    }
}

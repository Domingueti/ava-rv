﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class MenuTrigger : Menu {


	void OnTriggerEnter(Collider col) {
		if (col.tag.Equals ("Player")) {
            Debug.Log(col.gameObject.name);
			if (tag == "CoursePortal") {
                base.MenuPanel.GetComponent<ListView>().SetCourseContent();
                base.ShowMenu();
            } else if (tag == "ClassPortal") {
                //base.MenuPanel.GetComponent<ListView>().SetClassContent();
                base.ShowMenu();
            } else if (tag == "BackPortal") {
                NetManager.netSingleton.DisconnectClient();
            }
		}
	}

	void OnTriggerExit(Collider col) {
		if (col.tag.Equals ("Player") && base.menuPanel != null) {
			base.ShowMenu ();
		}
	}
}

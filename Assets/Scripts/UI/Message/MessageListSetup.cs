﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class MessageListSetup : MonoBehaviour {

	private List<Message> messageList;

	void OnEnable() {
		LoadMessages ();
	}

	//Get all from database
	void LoadMessages() {
		messageList = new List<Message> ();
		string content = "This is a text message containing text example";
		for (int i = 0; i < 12; i++) {
			string sender = "P_" + i.ToString ();
			string title = "Test " + i.ToString ();
			messageList.Add(new Message(title, content, sender, DateTime.Now));
		}
	}

	public List<Message> MessageList {
		get { return this.messageList; }
		set { this.messageList = value; }
	}
}

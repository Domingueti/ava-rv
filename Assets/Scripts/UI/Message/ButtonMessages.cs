﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonMessages : MonoBehaviour {

	private Message message;
	public Text textSender;
	public Text textTitle;
	private Menu messageMenu;

	public void Setup(Message message, GameObject menu) {
		messageMenu = GetComponent<Menu> ();
		this.message = message;
		textSender.text = message.Sender;
		textTitle.text = message.Title;
		messageMenu.MenuPanel = menu;
	}

	public void Action() {
		messageMenu.MenuPanel.GetComponent<MessageView> ().Setup (message);
		messageMenu.ShowMenu ();
	}

	public Message Message {
		get { return this.message; }
		set { this.message = value; }
	}

}

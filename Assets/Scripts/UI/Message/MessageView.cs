﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class MessageView : MonoBehaviour {

	public Text titleText;
	public Text senderText;
	public Text timeText;
	public Text contentText;

	public void Setup(Message message) {
		titleText.text = message.Title;
		senderText.text = message.Sender;
		timeText.text = System.DateTime.Now.ToString();
		contentText.text = message.Content;
		Debug.Log (message.ToString ());
	}


	public void Erase() {

	}

	public void Send() {
		//Message m = new Message (titleText.text, contentText.text, "Me", DateTime.Now);
		//Send message to player
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class QuitApp : MonoBehaviour {

    GazeTimedInput input;

    private void Start() {
        input = GetComponent<GazeTimedInput>();
        if (input != null) {
            input.OnGazeSelect = OnClick;
            if (GetComponent<Button>() != null)
                GetComponent<Button>().enabled = false;
        }
    }

    public void OnClick() {
        if (NetManager.netSingleton.IsClientConnected()) {
            NetManager.netSingleton.DisconnectClient();
        }
        Application.Quit();
    }
}

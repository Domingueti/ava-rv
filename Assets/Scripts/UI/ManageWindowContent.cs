﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageWindowContent : MonoBehaviour {
    public GameObject[] windows;
    private int currentWindow = 0;
    
    public void ShowWindow() {
        if (currentWindow == 0)
            ShowNext();
        else
            ShowPrevious();
    }


    void ShowNext() {
        windows[currentWindow].SetActive(false);
        currentWindow++;
        windows[currentWindow].SetActive(true);
    }

    void ShowPrevious() {
        windows[currentWindow].SetActive(false);
        currentWindow--;
        windows[currentWindow].SetActive(true);
    }
}

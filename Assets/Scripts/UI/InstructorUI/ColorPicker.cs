﻿using UnityEngine;

public class ColorPicker : MonoBehaviour {
    public static Color SelectedColor { get; set; }

    private void Start()
    {
        SelectedColor = Color.white;
    }

    public void OnSelectColor() {
        Debug.Log(gameObject.name);
        
        if (gameObject.name == "Blue")
            SelectedColor = Color.blue;
        else if (gameObject.name == "Red")
            SelectedColor = Color.red;
        else if (gameObject.name == "Green")
            SelectedColor = Color.green;
        else if (gameObject.name == "White")
            SelectedColor = Color.white;

        Debug.Log(SelectedColor);
    }
}

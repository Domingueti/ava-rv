﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupUsername : MonoBehaviour {

	// Use this for initialization
	void Start () {
        string userName = PlayerPrefs.GetString("Name", string.Empty);
        GetComponent<Text>().text = "Bem vindo " + userName + "!";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class PaintCanvas : NetworkBehaviour {

    private void Update() {
        if (Input.GetMouseButton(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                var pallet = hit.collider.GetComponent<PaintCanvas>();
                if (pallet != null) {

                    Renderer rend = hit.transform.GetComponent<Renderer>();
                    MeshCollider meshCollider = hit.collider as MeshCollider;

                    if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null) {
                        Debug.Log("Paint Returned");
                        return;
                    }
                    
                    Texture2D tex = rend.material.mainTexture as Texture2D;                    
                    Vector2 pixelUV = hit.textureCoord;

                    pixelUV.x *= tex.width;
                    pixelUV.y *= tex.height;
                    //Debug.Log("Send to Server");
                    if (isServer)
                        CmdPaintAreaWithColorOnServer(pixelUV, BrushSlider.BrushSize, ColorPicker.SelectedColor);
                }
            }
        }
    }

    [Command]
    void CmdPaintAreaWithColorOnServer(Vector2 pixelUV, int size, Color color) {
        //Debug.Log("Cmd Paint Area");
        RpcPaintAreaWithColorOnClients(pixelUV, size, color);
        //PaintAreaWithColor(pixelUV, size, color);
    }
    
    [ClientRpc]
    void RpcPaintAreaWithColorOnClients(Vector2 pixelUV, int size, Color color) {
        //Debug.Log("Rpc Recv");
        PaintAreaWithColor(pixelUV, size, color);
    }

    void PaintAreaWithColor(Vector2 pixelUV, int size, Color color) {
        Texture2D tex = (Texture2D) NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<Renderer>().material.mainTexture;
        for (int x = -size; x < size; x++) {
            for (int y = -size; y < size; y++) {
                tex.SetPixel((int)pixelUV.x + x, (int)pixelUV.y + y, color);
            }
        }
        tex.Apply();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class CreateButton : MonoBehaviour {

    public InputField nameInput;
    public InputField courseInput;
    public Dropdown roomType;

	public void OnCreateButton() {
        string name = nameInput.text;
        Course cr = new Course(courseInput.text);
        NetManager.netSingleton.CourseList.Add(cr);
        
        if (string.IsNullOrEmpty(name)) {
            SimpleMessageBox.singleton.DisplayText("O campo 'Nome' não pode ser vazio.");
        } else {
            string type = roomType.captionText.text;
            if (type.Equals("Sala Comum")) {
                NetworkHUD.singleton.OnClickCreateClassroom(name, true, "Classroom");
            } else if (type.Equals("Gallery Walk")) {
                NetworkHUD.singleton.OnClickCreateClassroom(name, true, "GalleryWalk");
            }
        }
    }

}

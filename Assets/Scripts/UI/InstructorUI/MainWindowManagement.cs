﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MainWindowManagement : MonoBehaviour {

    public InputField pathInput;
    SwitchVrMode switchMode;

	// Use this for initialization
	void Start () {
        string path = PlayerPrefs.GetString("path", Application.temporaryCachePath);
        PlayerPrefs.SetString("path", path);
        pathInput.text = path;
        switchMode = GetComponent<SwitchVrMode>();
    }
	
    public void EnterWorld() {
        NetManager.netSingleton.SetStudent();
        switchMode.LoadVrScene("MainScene");
    }

    public void QuitApp() {
        Application.Quit();
    }

    public void SetFolderPath() {
        DirectoryInfo di = new DirectoryInfo(pathInput.text);
        if (!di.Exists) {
            SimpleMessageBox.singleton.DisplayText("Diretório não encontrado! Digite um diretório válido.");
        } else {
            PlayerPrefs.SetString("path", pathInput.text);
        }
        
    }
}

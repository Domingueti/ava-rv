﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class EditMode : MonoBehaviour {

    private byte[] oldTexture;
    private byte[] newTexture;
    public Texture2D currentTexture;
    public GameObject boardTemp;

    private void Start() {
        StartCoroutine("SaveFile", true);
    }

    public void OnSaveFile() {
        StartCoroutine("SaveFile", false);
    }

    private IEnumerator SaveFile(bool temporaryFolder) {
        newTexture = null;
        Texture tex = NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<Renderer>().material.mainTexture;
        StartCoroutine(GetTextureBytes(tex));
        yield return new WaitUntil(() => newTexture != null);
        string fileName = System.DateTime.Now.ToString("yyyyMMddHHmmss");
        string path = string.Empty;
        if (temporaryFolder) {
            path = Application.temporaryCachePath + "oldTexture.png";
        } else {
            path = FileListUI.FolderPath + fileName + ".png";
        }

        if (!temporaryFolder) {
            try {
                File.WriteAllBytes(path, newTexture);
                SimpleMessageBox.singleton.DisplayText("Arquivo salvo em: " + path);
            } catch (FieldAccessException e) {
                SimpleMessageBox.singleton.DisplayText("Não foi possível salvar na pasta destinada. Você não possui credenciais para salvar.");
                Debug.Log(e.ToString());
            } catch (DirectoryNotFoundException e) {
                SimpleMessageBox.singleton.DisplayText("Diretório não encontrado. Os arquivos são salvos no mesmo diretório em que os recursos estão.");
                Debug.Log(e.ToString());
            } catch (IOException e) {
                SimpleMessageBox.singleton.DisplayText("Houve um erro ao salvar o arquivo. Tente novamente mais tarde.");
                Debug.Log(e.ToString());
            }
        }
    }

    public void OnCancelEditing() {
        NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().LoadContent(Application.temporaryCachePath + "oldTexture.png");
    }

    public void OnNewBlankTexture() {
        NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().LoadContent("blank");
    }

    IEnumerator GetTextureBytes(Texture texture) {
        yield return new WaitForEndOfFrame();
        currentTexture = (Texture2D) texture;
        newTexture = currentTexture.EncodeToPNG();
    }
}

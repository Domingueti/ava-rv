﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEditMenu : Menu {

    MonoBehaviour previousContent;
    uint previousType;

    public override void ShowMenu() {
        GameObject localRef = NetManager.netSingleton.boardManager.GetActiveBoard();

        if (localRef.GetComponent<ContentView>().IsLoading)
            return;

        if (!MenuPanel.activeInHierarchy) {
            uint type = localRef.GetComponent<ContentView>().Type;
            previousContent = localRef.GetComponent<ContentView>().contentManager as MonoBehaviour;
            previousType = localRef.GetComponent<ContentView>().Type;
            localRef.GetComponent<PaintCanvas>().enabled = true;
            if (localRef.GetComponent<ContentView>().Type != (uint)EnumContentType.ContentType.TEXTURE) {
                previousContent.enabled = false;
                localRef.GetComponent<ContentView>().StoreStatus();
                localRef.GetComponent<ContentView>().SetTextureContent(false);
            }

            TextureContent content = (TextureContent)localRef.GetComponent<ContentView>().contentManager;
            content.isEditing = true;
        } else {
            StartCoroutine(DisableEditMode());
        }
        base.ShowMenu();
        localRef.GetComponent<ContentView>().ShowParcial = menuPanel.activeInHierarchy;
    }

    IEnumerator DisableEditMode() {
        GameObject localRef = NetManager.netSingleton.boardManager.GetActiveBoard();
        localRef.GetComponent<PaintCanvas>().enabled = false;

        yield return new WaitForEndOfFrame();

        if (previousType != (uint)EnumContentType.ContentType.TEXTURE) {
            Destroy(localRef.GetComponent<TextureContent>());
        } else {
            TextureContent content = (TextureContent)localRef.GetComponent<ContentView>().contentManager;
            content.isEditing = false;
        }
        previousContent.enabled = true;
        
        localRef.GetComponent<ContentView>().contentManager = previousContent.GetComponent<IContentManager>();
        localRef.GetComponent<ContentView>().Type = previousType;
        localRef.GetComponent<ContentView>().ResetButtonsStatus();
        NetManager.netSingleton.boardManager.UpdateCurrentBoard();
    }

}

﻿using UnityEngine;
using UnityEngine.UI;

public class BrushSlider : MonoBehaviour {

    public static int BrushSize { get; set; }
    Slider slider;

	void Start () {
        BrushSize = 1;
        slider = GetComponent<Slider>();
    }
	
	public void OnValueChanged() {
        BrushSize = (int) slider.value;
    }
}

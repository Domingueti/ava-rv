﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour {

    private List<Camera> cameraList;
    [SerializeField] private int current = 0;

    public void SetupCameraList(GameObject camListObject) {
        GameObject teacher = GameObject.FindGameObjectWithTag("PlayerTeacher");
        Camera cam = teacher.GetComponentInChildren<Camera>();

        cameraList = new List<Camera>();
        cameraList.Add(cam);
        cam.gameObject.SetActive(true);
        Camera[] list = camListObject.GetComponentsInChildren<Camera>();
        for (int i = 0; i < list.Length; i++) {
            list[i].tag = "MainCamera";
            list[i].gameObject.SetActive(false);
            cameraList.Add(list[i]);
        }
        current = 0;
    }

    public void NextCamera() {
        if (NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<PaintCanvas>().enabled ||
            NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().IsLoading)
            return;

        cameraList[current].gameObject.SetActive(false);
        if (current >= cameraList.Count - 1) {
            current = 0;
        } else {
            current++;
            NetManager.netSingleton.boardManager.OnBoardChange(current - 1);
        }
        Debug.Log("Current Cam: " + current);
        Debug.Log("Current Board: " + NetManager.netSingleton.boardManager.CurrentBoard);
        
        cameraList[current].gameObject.SetActive(true);
    }
}

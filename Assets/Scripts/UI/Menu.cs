﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	[SerializeField] protected GameObject menuPanel;
    private GazeTimedInput input;

    private void Start() {
       if (XRSettings.enabled) {
        input = GetComponent<GazeTimedInput>();
        if (input != null)
            input.OnGazeSelect += ShowMenu;
        }
    }

    //Habilita/Desabilita a exibicao do menu passado como parametro
    public virtual void ShowMenu() {        
		menuPanel.SetActive (!menuPanel.activeInHierarchy);
	}

	//Acessor
	public GameObject  MenuPanel {
		get { return this.menuPanel; }
		set { this.menuPanel = value; }
	}
}

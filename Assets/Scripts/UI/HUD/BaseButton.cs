﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseButton : MonoBehaviour {

    public Sprite enableSprite;
    public Sprite disableSprite;
    public bool State { get; set; }

    protected virtual void Start() {
        State = false;
        ChangeImage();
    }

    public virtual void OnClick() {
        State = !State;
        ChangeImage();
    }

    public virtual void ChangeImage() {
        if (State)
            GetComponent<Image>().sprite = enableSprite;
        else
            GetComponent<Image>().sprite = disableSprite;
    }
}

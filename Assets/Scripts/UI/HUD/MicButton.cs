﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;

public class MicButton : BaseButton {

    public delegate void MicClicked(bool value);
    public static event MicClicked OnMicClicked;
    private GazeTimedInput input;

    protected override void Start() {
        base.Start();
    }

    public override void OnClick() {
            if (OnMicClicked != null)
                OnMicClicked(State);
    }

    public void SetOnlineMic() {
        input = GetComponent<GazeTimedInput>();
        input.OnGazeSelect += OnClick;
        if (GetComponent<Button>() != null)
            GetComponent<Button>().enabled = false;
    }
}

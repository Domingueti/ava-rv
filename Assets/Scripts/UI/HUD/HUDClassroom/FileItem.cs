﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileItem : MonoBehaviour {

    public string FileName { get; set; }
    public string Extension { get; set; }
    public string FilePath { get; set; }

    public void OnClick() {
        if (!NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().IsLoading) {
            if (Extension == ".pdf") {
                SimpleMessageBox.singleton.DisplayText("Aguarde, carregando o arquivo ...");
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().SetSlidesContent(true);
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().LoadContent(FilePath);
            } else if (Extension == ".url") { //video online url
                string url = File.ReadAllText(FilePath);
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().SetVideoContent(true);
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().LoadContent(url);
            } else if (Extension == ".jpg" || Extension == ".png") {
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().SetTextureContent(true);
                NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<ContentView>().LoadContent(FilePath);
            }
        } else {
            SimpleMessageBox.singleton.DisplayText("Operação não pode ser realizada. Um conteúdo já está sendo carregado.");
        }
    }

    public void Setup(string fileName, string filePath, string extension) {
        FileName = fileName;
        FilePath = filePath;
        Extension = extension;
        GetComponentInChildren<Text>().text = FileName;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeFileList : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GazeTimedInput input = GetComponent<GazeTimedInput>();
        FileListUI fileList = GetComponent<FileListUI>();
        input.OnGazeSelect += fileList.ShowFileList;
	}
}

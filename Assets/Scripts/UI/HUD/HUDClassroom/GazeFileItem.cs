﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeFileItem : MonoBehaviour {

    // Use this for initialization
    void Start() {
        GazeTimedInput input = GetComponent<GazeTimedInput>();
        FileItem file = GetComponentInParent<FileItem>();
        input.OnGazeSelect += file.OnClick;
    }

}

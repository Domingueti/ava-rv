﻿using UnityEngine;
using UnityEngine.Video;

public class VideoNextAction : MonoBehaviour, INextAction {

    public VideoPlayer videoPlayer;

    public void NextAction() {
        if (videoPlayer.frame + 25 > (long)videoPlayer.frameCount)
            videoPlayer.frame = (long)videoPlayer.frameCount;
        else
            videoPlayer.frame += 25;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidesNextAction : MonoBehaviour, INextAction {

    public SlidesContent content;

    public void NextAction() {
        if (content.HasNext()) {
            content.CurrentIndex++;
            content.gameObject.GetComponent<Renderer>().material.mainTexture = content.Slides[content.CurrentIndex];
        }
    }
}

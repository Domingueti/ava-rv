﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextureContent : MonoBehaviour, IContentManager {

    public Texture2D currentTexture;
    private byte[] newTexture;
    public ContentBaseMsg ContentMessage { get; set; }
    public bool isEditing = false;
    
    public void LoadContent(string path) {
        /*if (path == "blank") {
            currentTexture = Resources.Load<Texture2D>("blackboard");
        } else {
            byte[] data = File.ReadAllBytes(path);
            currentTexture = null;
            currentTexture = new Texture2D(2, 2);
            currentTexture.LoadImage(data);
        }*/
        if (path == "1")
        {
            currentTexture = Resources.Load<Texture2D>("vr1");
        } else if (path == "2")
        {
            currentTexture = Resources.Load<Texture2D>("vr2");
        } else if (path == "3")
        {
            currentTexture = Resources.Load<Texture2D>("vr3");
        }
        else if (path == "4")
        {
            currentTexture = Resources.Load<Texture2D>("vr4");
        }

    }

    public IEnumerator SendData() {
        newTexture = null;
        StartCoroutine(GetTextureBytes(currentTexture));
        yield return new WaitUntil (() => newTexture != null);
        ContentMessage = null;
        ContentMessage = new ContentTextureMsg(newTexture, isEditing);
        ContentMessage.type = (uint)EnumContentType.ContentType.TEXTURE;
    }

    public void SyncContent(ContentBaseMsg contentMessage) {
        ContentTextureMsg msg = (ContentTextureMsg)contentMessage;
        if (msg.isEditing)
            GetComponent<PaintCanvas>().enabled = msg.isEditing;

        Debug.Log(msg);
        currentTexture = new Texture2D(2, 2);
        currentTexture.LoadImage(msg.Texture);
        currentTexture.Apply();
        GetComponent<Renderer>().material.mainTexture = currentTexture;
    }

    IEnumerator GetTextureBytes(Texture2D texture) {
        yield return new WaitForEndOfFrame();
        currentTexture = texture;
        newTexture = currentTexture.EncodeToJPG();
    }
}

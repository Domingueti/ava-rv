﻿using System.Collections;

public interface IContentManager {

    ContentBaseMsg ContentMessage { get; set; }

    void LoadContent(string path);
    IEnumerator SendData();
    void SyncContent(ContentBaseMsg contentMessage);
}

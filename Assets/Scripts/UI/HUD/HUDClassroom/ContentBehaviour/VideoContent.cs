﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoContent : MonoBehaviour, IContentManager {
    private ContentBaseMsg contentMessage;

    public VideoPlayer videoPlayer;
    public ContentBaseMsg ContentMessage { get { return this.contentMessage; } set { this.contentMessage = value; } }

    public void LoadContent(string path) {
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.controlledAudioTrackCount = 1;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, GetComponent<AudioSource>());

        videoPlayer.url = path;
        videoPlayer.errorReceived += OnErrorReceived;
        videoPlayer.prepareCompleted += OnVideoPrepared;
        videoPlayer.Prepare();
    }

    public void OnVideoPrepared(VideoPlayer source) {
        source.Play();
    }

    public void OnErrorReceived(VideoPlayer source, string message) {
        SimpleMessageBox.singleton.DisplayText(message);
    }

    public IEnumerator SendData() {
        ContentMessage = new ContentVideoMsg(videoPlayer.frame, videoPlayer.url, videoPlayer.isPlaying);
        ContentMessage.type = (uint)EnumContentType.ContentType.VIDEO;
        yield return null;
    }

    public void SyncContent(ContentBaseMsg contentMessage) {
        ContentVideoMsg msg = (ContentVideoMsg)contentMessage;
        Debug.LogWarning("Msg Recv: " + msg.ToString());
        videoPlayer.url = msg.Url;
        if (msg.CurrentFrame - videoPlayer.frame > 15)
            videoPlayer.frame = msg.CurrentFrame;
        if (msg.isPlaying && !videoPlayer.isPlaying) {
            videoPlayer.frame = msg.CurrentFrame;
            videoPlayer.Play();
        }
    }
}

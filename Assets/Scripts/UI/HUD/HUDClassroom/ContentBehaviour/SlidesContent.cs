﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Diagnostics;
public class SlidesContent : MonoBehaviour, IContentManager {

    private List<Texture2D> slides;

    private byte[] newTexture;
    private Texture2D currentSlide;
    private ContentBaseMsg contentMessage;

    public List<Texture2D> Slides { get { return this.slides; } set { this.slides = value; } }
    public int CurrentIndex { get; set; }

    public ContentBaseMsg ContentMessage {
        get { return this.contentMessage; }
        set { this.contentMessage = value; }
    }

    public bool HasNext() { return CurrentIndex < Slides.Count - 2; } //has a next slide
    public bool HasPrevious() { return CurrentIndex > 0; } //has a previous slide
    public bool HasLoaded { get; set; } //has the PDF content finished loading?

    public void LoadContent(string path) {
        HasLoaded = false;
        slides = new List<Texture2D>();
        if (slides != null)
            slides.Clear();
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
            StartCoroutine("LoadPdf", path);
        else
            SimpleMessageBox.singleton.DisplayText("Arquivos PDF suportados apenas em ambientes Windows.");
    }

    private IEnumerator LoadPdf(string fileName) {
        UnityEngine.Debug.Log(Application.temporaryCachePath);
        Process process = null;
        string path = Application.temporaryCachePath + "/" + fileName.Substring(FileListUI.FolderPath.Length);
        
        DirectoryInfo di = new DirectoryInfo(path);
        FileInfo fi = new FileInfo(fileName);

        if (fi.LastWriteTime > di.LastWriteTime) {
            if (!di.Exists) {
                Directory.CreateDirectory(path);
            } else {
                foreach (FileInfo file in di.EnumerateFiles()) {
                    file.Delete();
                }
            }
            SimpleMessageBox.singleton.DisplayText("Aguarde a conversão do arquivo PDF ...");
            process = new Process();
            process.StartInfo.FileName = Path.Combine(Application.streamingAssetsPath, "mutool.exe");
            process.StartInfo.Arguments = "convert -F png -o " + path + "/output%d.png " + fileName;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            yield return new WaitUntil(() => process.HasExited);
        } else {
            yield return null;
        }
        
        //if the process has terminated or wasn't executed
        if (process == null || (process != null && process.ExitCode == 0)) {
            SimpleMessageBox.singleton.DisplayText("Carregando imagens ...");
            slides = new List<Texture2D>();
            string[] names = Directory.GetFiles(path);
            for (int i = 0; i < names.Length; i++) {
                Texture2D tex = new Texture2D(1, 1);
                byte[] data = File.ReadAllBytes(names[i]);
                tex.LoadImage(data);
                slides.Add(tex);
            }

            currentSlide = slides[0];
            CurrentIndex = 0;
            GetComponent<Renderer>().material.mainTexture = slides[0];
            HasLoaded = true;
        } else {
            SimpleMessageBox.singleton.DisplayText("Houve um erro ao converter ou abrir o arquivo.");
        }
    }

    public IEnumerator SendData() {
        newTexture = null;
        yield return new WaitForEndOfFrame();
        newTexture = slides[CurrentIndex].EncodeToJPG();
        contentMessage = null;
        contentMessage = new ContentSlidesMsg(CurrentIndex, newTexture);
        contentMessage.type = (uint)EnumContentType.ContentType.SLIDE;
    }

    public void SyncContent(ContentBaseMsg contentMessage) {
        ContentSlidesMsg msg = (ContentSlidesMsg)contentMessage;
        currentSlide = null;
        currentSlide = new Texture2D(10, 10);
        currentSlide.LoadImage(msg.Texture);
        currentSlide.Apply();
        GetComponent<Renderer>().material.mainTexture = currentSlide;
        CurrentIndex = msg.Index;
    }
}


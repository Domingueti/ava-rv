﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileShowMenu : Menu {

    public override void ShowMenu() {
        if (!NetManager.netSingleton.boardManager.GetActiveBoard().GetComponent<PaintCanvas>().enabled)
            base.ShowMenu();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayAction : MonoBehaviour, IPlayAction {

    public VideoPlayer videoPlayer;

    public void PlayAction() {
        if (videoPlayer.isPlaying)
            videoPlayer.Pause();
        else
            videoPlayer.Play();
    }
}

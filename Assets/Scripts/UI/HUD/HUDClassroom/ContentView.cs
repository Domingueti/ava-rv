﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Video;

public class ContentView : NetworkBehaviour {

    private INextAction nextAction = null;
    private IPreviousAction previousAction = null;
    private IPlayAction playAction = null;
    
    public GameObject previousButton;
    public GameObject nextButton;
    public GameObject playButton;
    public GameObject statusObject;

    public IContentManager contentManager;
    private NetworkTransmitter transmitter;
    private bool compressMessage = false;
    public bool IsSet { get; set; }
    public bool IsLoading { get; set; }
    public bool ShowParcial { get; set; } //show the parcial content on the host client
    public uint Type { get; set; }

    [SerializeField] private bool[] previousStatus;
    public override void OnStartServer() {
        previousStatus = new bool[3];
        if (isServer) {
            ShowParcial = true;
           /* GameObject hud = GameObject.FindGameObjectWithTag("HUDClassroom");
            if (hud != null) {
                foreach (Transform t in hud.GetComponentInChildren<Transform>()) {
                    if (t.gameObject.name.Equals("Button_Previous")) {
                        previousButton = t.gameObject;
                    }

                    if (t.gameObject.name.Equals("Button_PlayPause")) {
                        playButton = t.gameObject;
                    }

                    if (t.gameObject.name.Equals("Button_Next")) {
                        nextButton = t.gameObject;
                    }
                }
            }*/
            SetTextureContent(true);
            if (gameObject.name == "Board 1")
                LoadContent("1");
            else if (gameObject.name == "Board 2")
                LoadContent("2");
            else if (gameObject.name == "Board 3")
                LoadContent("3");
            else if (gameObject.name == "Board 4")
                LoadContent("4");
        }
    }
    
    private void Start() {
        statusObject = gameObject.transform.GetChild(0).gameObject;
        statusObject.SetActive(false);

        transmitter = GetComponent<NetworkTransmitter>();
        transmitter.OnDataCompletelyReceived += SyncContent;
        transmitter.OnDataFragmentReceived += SyncParcialContent;
    }

    public void OnNextButton() {
        if (nextAction != null) {
            nextAction.NextAction();
            StartCoroutine("SendData");
        }
    }

    public void OnPreviousButton() {
        if (previousAction != null) {
            previousAction.PreviousAction();
            StartCoroutine("SendData");
        }
    }

    public void OnPlayButton() {
        if (playAction != null) {
            playAction.PlayAction();
            StartCoroutine("SendData");
        }
    }

    public void LoadContent(string path) {
        if (path.Contains(".pdf")) {
            StartCoroutine("LoadAndSendContent", path);
        } else {
            contentManager.LoadContent(path);
            StartCoroutine("SendData");
        }
    }

    private IEnumerator LoadAndSendContent(string path) {
        StoreStatus();
        IsLoading = true;
        SetHudController(false);
        statusObject.SetActive(true);
        contentManager.LoadContent(path);
        SlidesContent slidesC = (SlidesContent)contentManager;
        yield return new WaitUntil(() => slidesC.HasLoaded);
        StartCoroutine("SendData");
    }

    //Create the Package and send to clients over RpcClient Call
    [Server]
    public IEnumerator SendData() {
        IsLoading = true;
        if (nextButton != null)
            nextButton.SetActive(false);
        if (previousButton != null)
            previousButton.SetActive(false);
        if (playButton != null)
            playButton.SetActive(false);

        StartCoroutine(contentManager.SendData());
        yield return new WaitUntil(() => contentManager.ContentMessage != null);

        byte[] message = ByteOperations.ObjectToByteArray(contentManager.ContentMessage);
        
        if (compressMessage)
            message = ByteOperations.CompressData(message);

        Debug.LogWarning(message.Length);
        transmitter.SendBytesToClients(0, message);
    }


    //Syncronizes the content received from the server
    [Client]
    public void SyncContent(int tranmissionId, byte[] msg) {
        statusObject.SetActive(false);
        ResetButtonsStatus();

        if (compressMessage)
            msg = ByteOperations.DecompressData(msg);

        ContentBaseMsg message = (ContentBaseMsg) ByteOperations.ByteArrayToObject(msg);
        if (Type == (uint)EnumContentType.ContentType.NONE || (uint)message.type != Type) {
            if (message.type == (uint) EnumContentType.ContentType.SLIDE) {
                SetSlidesContent(true);
            } else if (message.type == (uint) EnumContentType.ContentType.VIDEO) {
                SetVideoContent(true);
            } else if (message.type == (uint) EnumContentType.ContentType.TEXTURE) {
                SetTextureContent(true);
            }
        }
        contentManager.SyncContent(message);
        IsLoading = false;
    }   

    [Client]
    public void SyncParcialContent(int transmissionId, byte[] data) {
        if (ShowParcial) {
            GetComponent<Renderer>().material.mainTexture = null;
            statusObject.SetActive(true);
        }
    }

    public void SetSlidesContent(bool destroyContent) {
        if (destroyContent && contentManager != null)
            DestroyContentManager();

        SlidesContent content = gameObject.AddComponent<SlidesContent>();
        contentManager = content;
        compressMessage = true;
        Type = (uint)EnumContentType.ContentType.SLIDE;
        if (isServer) {
            SlidesPreviousAction previous = gameObject.AddComponent<SlidesPreviousAction>();
            SlidesNextAction next = gameObject.AddComponent<SlidesNextAction>();
            if (playButton != null)
                playButton.SetActive(false);
            if (nextButton != null)
                nextButton.SetActive(true);
            if (previousButton != null)
            previousButton.SetActive(true);

            StoreStatus();

            previous.content = content;
            next.content = content;
            nextAction = next;
            previousAction = previous;
            SetButtonsActions();
        }
        IsSet = true;
    }

    public void SetVideoContent(bool destroyContent) {
        if (destroyContent && contentManager != null)
            DestroyContentManager();

        Type = (uint)EnumContentType.ContentType.VIDEO;

        VideoPlayer player = GetComponent<VideoPlayer>();
        if (player == null) {
            player = gameObject.AddComponent<VideoPlayer>();
        }
        VideoContent videoContent = gameObject.AddComponent<VideoContent>();
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        player.SetTargetAudioSource(0, audioSource);
        videoContent.videoPlayer = player;
        contentManager = videoContent;
        
        if (isServer) {
            VideoNextAction next = gameObject.AddComponent<VideoNextAction>();
            VideoPreviousAction previous = gameObject.AddComponent<VideoPreviousAction>();
            VideoPlayAction play = gameObject.AddComponent<VideoPlayAction>();
            
            next.videoPlayer = videoContent.videoPlayer;
            previous.videoPlayer = videoContent.videoPlayer;
            play.videoPlayer = videoContent.videoPlayer;

            nextAction = next;
            previousAction = previous;
            playAction = play;
            if (playButton != null)
                playButton.SetActive(true);
            if (nextButton != null)
                nextButton.SetActive(true);
            if (previousButton != null)
                previousButton.SetActive(true);

            StoreStatus();
            SetButtonsActions();
        }
        IsSet = true;
    }

    public void SetTextureContent(bool destroyContent) {
        if (destroyContent && contentManager != null)
            DestroyContentManager();

        Type = (uint)EnumContentType.ContentType.TEXTURE;
        
        TextureContent texContent = gameObject.AddComponent<TextureContent>();
        contentManager = texContent;
        if (isServer) {
            SetHudController(false);
            if (destroyContent)
                StoreStatus();
        }
        IsSet = true;
    }

    public void DestroyContentManager() {
        if (nextAction != null)
            Destroy(nextAction as Component);
        if (previousAction != null)
            Destroy(previousAction as Component);
        if (playAction != null)
            Destroy(playAction as Component);
        if (contentManager != null)
            Destroy(contentManager as Component);
        if (GetComponent<VideoPlayer>() != null)
            Destroy(GetComponent<VideoPlayer>());
        if (GetComponent<AudioSource>() != null)
            Destroy(GetComponent<AudioSource>());
        IsSet = false;
        Type = (uint)EnumContentType.ContentType.NONE;
    }
    
    
    //Content Buttons (Previous, Play, Next) Management and Setup
    void SetHudController(bool state) {
        if (previousButton != null)
            previousButton.SetActive(state);
        if (nextButton != null)
            nextButton.SetActive(state);
        if (playButton != null)
            playButton.SetActive(state);
    }

    public void SetButtonsActions() {
        nextButton.GetComponent<Button>().onClick.RemoveAllListeners();
        previousButton.GetComponent<Button>().onClick.RemoveAllListeners();
        playButton.GetComponent<Button>().onClick.RemoveAllListeners();

        if (nextButton.activeInHierarchy)
            nextButton.GetComponent<Button>().onClick.AddListener(OnNextButton);
        if (previousButton.activeInHierarchy)
            previousButton.GetComponent<Button>().onClick.AddListener(OnPreviousButton);
        if (playButton.activeInHierarchy)
            playButton.GetComponent<Button>().onClick.AddListener(OnPlayButton);
    }

    public void StoreStatus() {
        if (previousButton != null)
            previousStatus[0] = previousButton.activeInHierarchy;
        if (playButton != null)
            previousStatus[1] = playButton.activeInHierarchy;
        if (nextButton != null)
            previousStatus[2] = nextButton.activeInHierarchy;
    }

    public void ResetButtonsStatus() {
        if (previousButton != null)
            previousButton.SetActive(previousStatus[0]);
        if (playButton != null)
            playButton.SetActive(previousStatus[1]);
        if (nextButton != null)
            nextButton.SetActive(previousStatus[2]);
    }
}

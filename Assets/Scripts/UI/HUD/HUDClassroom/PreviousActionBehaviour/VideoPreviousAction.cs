﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPreviousAction : MonoBehaviour, IPreviousAction {

    public VideoPlayer videoPlayer;

    public void PreviousAction() {
        if (videoPlayer.frame - 25 < 0)
            videoPlayer.frame = 0;
        else
            videoPlayer.frame -= 25;
    }
}

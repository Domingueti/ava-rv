﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidesPreviousAction : MonoBehaviour, IPreviousAction {

    public SlidesContent content;

    public void PreviousAction() {
        if (content.HasPrevious()) {
            content.CurrentIndex--;
            content.gameObject.GetComponent<MeshRenderer>().material.mainTexture = content.Slides[content.CurrentIndex];
        }
        
    }
}

﻿using UnityEngine;
using System.IO;

public class FileListUI : MonoBehaviour {

    public static string FolderPath { get; set; }

    public GameObject content;
    public GameObject fileItemPrefab;

    // Use this for initialization
    private void OnEnable() {
        if (string.IsNullOrEmpty(FolderPath)) {
            FolderPath = PlayerPrefs.GetString("path");
        }
        ShowFileList();
    }

    public void ShowFileList() {
        Debug.Log(FolderPath);
        DirectoryInfo di = new DirectoryInfo(FolderPath);

        if (!di.Exists) {
            SimpleMessageBox.singleton.DisplayText("Não foi possível listar os arquivos. O diretório especificado não existe.");
        } else {

            string[] fileList = Directory.GetFiles(FolderPath);
            DeleteFileList();
            if (fileList.Length == 0) {
                SimpleMessageBox.singleton.DisplayText("Não há arquivos para serem exibidos.");
            } else {
                for (int i = 0; i < fileList.Length; i++) {
                    Debug.Log(fileList[i]);
                    string fileName = fileList[i].Substring(FolderPath.Length);
                    string extension = Path.GetExtension(fileList[i]);
                    GameObject fileItem = (GameObject)Instantiate(fileItemPrefab);
                    fileItem.GetComponent<FileItem>().Setup(fileName, fileList[i], extension);
                    fileItem.transform.SetParent(content.transform);
                }
            }
        }
    }

    private void DeleteFileList() {
        FileItem[] childs = content.GetComponentsInChildren<FileItem>();
        for (int i = 0; i < childs.Length; i++)
            if (childs[i].gameObject != null)
                Destroy(childs[i].gameObject);
    }
}

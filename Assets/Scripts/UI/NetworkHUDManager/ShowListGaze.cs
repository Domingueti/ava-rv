﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowListGaze : MonoBehaviour {

    public ListView listView;
    private GazeTimedInput input;
	// Use this for initialization
	void Start () {
        input = GetComponent<GazeTimedInput>();
        if (input != null)
            input.OnGazeSelect += listView.ShowList;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking.Match;
using UnityEngine.Networking;

public class RoomItem : MonoBehaviour {
    
    //UI Reference elements
    [SerializeField] Button joinButton;
	[SerializeField] Text roomName;
	[SerializeField] Text amount;

    //Attrs and Utils
    [SerializeField] private IJoinButton joinAction;
	private MatchInfoSnapshot room;
    private Course course;

	public void Setup(MatchInfoSnapshot match) {
		this.room = match;
		roomName.text = room.name;
		UpdateAmount (room.currentSize, room.maxSize);
	}

	public void Setup(string name) {
		roomName.text = name;
		this.room = null;
		UpdateAmount (0, 0);
	}

	public void Setup(Course course) {
        this.course = course;
        roomName.text = course.Name;
		this.room = null;
		UpdateAmount (0, 0);
	}

    public void UpdateAmount(int curr, int max) {
        if (max == 0) {
            amount.text = "-- / --";
        } else {
            amount.text = string.Format("{0} / {1}", room.currentSize, room.maxSize);
        }
    }

    public void OnJoinButton() {
        if (room == null) {
            Debug.Log(joinAction == null);
            if (joinAction.GetType() == typeof(CourseJoinButton)) {
                joinAction.OnJoinButton(room, course.Name);
            }
            else {
                if (roomName.text.Equals("Sala Privada")) {
                    joinAction.OnJoinButton(room, "Sala Privada " + NetManager.netSingleton.CurrentUser.Name);
                }
                else {
                    joinAction.OnJoinButton(room, RoomManager.GetClassroomName(course.Name, NetManager.netSingleton.CurrentUser.Name));
                }
            }
        } else {
            joinAction.OnJoinButton(room, room.name);
        }
    }

    public void SetCourseButtonAction() {
        joinAction = gameObject.AddComponent<CourseJoinButton>();
        GetComponent<CourseJoinButton>().Course = course;
    }

    public void SetClassroomButtonAction() {
        DestroyJoinBehaviour();
        joinAction = gameObject.AddComponent<ClassroomJoinButton>();
    }

    void DestroyJoinBehaviour() {
        if (joinAction != null) {
            var obj = GetComponent<IJoinButton>() as Component;
            Destroy(obj);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.XR;

public class NetworkHUD : MonoBehaviour {
	public GameObject itemPrefab;

    public static NetworkHUD singleton;

    private void Awake() {
        if (singleton != null)
            Destroy(this.gameObject);
        else
            singleton = this;
    }

    #region OnClickEvents
    public void OnClickListMatches(string filter) {
		if (string.IsNullOrEmpty (filter)) {
            filter = string.Empty;
		}
		NetManager.netSingleton.matchMaker.ListMatches (0, 10, filter, true, 0, 0, OnMatchList);
	}

	public void OnClickCreateCourse(string name, bool advertise) {
		NetManager.netSingleton.onlineScene = "CourseLobby";
        NetManager.netSingleton.matchMaker.CreateMatch (name, 7, advertise, "", "", "", 0, 0, OnMatchCreate);
	}

	public void OnClickCreateClassroom(string name, bool advertise, string roomType) {
        NetManager.netSingleton.StartMatchMaker ();
        NetManager.netSingleton.onlineScene = roomType;
        NetManager.netSingleton.matchMaker.CreateMatch (name, 7, advertise, "", "", "", 0, 0, OnMatchCreate);
	}

	public void OnClickJoinMatch(MatchInfoSnapshot match,string password) {
        NetManager.netSingleton.CurrentMatch = match;
        NetManager.netSingleton.matchMaker.JoinMatch (match.networkId, password, "", "", 0, 0, OnMatchJoined);
	}
	#endregion

	#region Callbacks
	//Exibe as partidas na tela
	private void OnMatchList (bool success, string extendedInfo, List<MatchInfoSnapshot> matchList) {
		if (success) {
            NetManager.netSingleton.matches = matchList;
			foreach (MatchInfoSnapshot match in matchList) {
				GameObject itemListPrefab = Instantiate (itemPrefab);
                Debug.Log(match.name);
				itemListPrefab.GetComponent<RoomItem> ().Setup (match);
                if (match.name.Contains("_"))
                    itemListPrefab.GetComponent<RoomItem>().SetClassroomButtonAction();
                else
                    itemListPrefab.GetComponent<RoomItem>().SetCourseButtonAction();
				itemListPrefab.transform.SetParent (ListView.contentPanel.transform, false);
			}
		} else {
			Debug.Log ("Failed to list matches: " + extendedInfo);
            SimpleMessageBox.singleton.DisplayText("Não foi possível listas as salas. Verifique sua conexão com a internet.");
        }
	}

	//Cria uma partida
	private void OnMatchCreate (bool success, string extendedInfo, MatchInfo matchInfo) {
		Debug.Log ("On Match Create");
		if (success) {
            MatchInfo host = matchInfo;
            NetworkServer.Listen(host, 9000);
            NetworkServer.Reset();
            NetManager.netSingleton.StartHost(host);
        } else {
			Debug.LogError ("Failed to create match: " + extendedInfo);
            SimpleMessageBox.singleton.DisplayText("Não foi possível criar uma sala.");
        }
	}

	private void OnMatchJoined (bool success, string extendedInfo, MatchInfo matchInfo) {
		if (success) {
            NetManager.netSingleton.StartClient (matchInfo);
		} else {
            NetManager.netSingleton.CurrentMatch = null;
			Debug.LogError ("Failed to Join Match " + extendedInfo);
            SimpleMessageBox.singleton.DisplayText("Não foi possível entrar na sala escolhida.");
        }
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour {
    //Gerencia as salas criadas e mantidas pelo usuário, bem como as salas das disciplinas disponíveis
    //no mundo virtual


    //Inicializa: Busca pela API os cursos
    public static void SetupList(List<Course> courseList) {
        courseList.Add(new Course("Teste"));
        courseList.Add(new Course("Teste2"));
    }

    public static string GetClassroomName(string courseName, string userName) {
        return courseName + "Sala de aula de " + userName;
    }
}

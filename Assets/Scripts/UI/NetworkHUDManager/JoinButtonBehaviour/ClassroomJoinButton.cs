﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;


public class ClassroomJoinButton : MonoBehaviour, IJoinButton {

    public void OnJoinButton(MatchInfoSnapshot room, string roomName) {
        if (room == null) {
            NetworkHUD.singleton.OnClickCreateClassroom(roomName, true, "Classroom");
        } else {
            NetManager.netSingleton.CurrentMatch = room;
            NetworkHUD.singleton.OnClickJoinMatch(room, "");
        }
    }

    
}

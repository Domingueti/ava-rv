﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking.Match;
public interface IJoinButton {

	void OnJoinButton (MatchInfoSnapshot room, string roomName);
}


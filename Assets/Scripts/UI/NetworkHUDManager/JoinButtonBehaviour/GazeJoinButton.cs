﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeJoinButton : MonoBehaviour {
    RoomItem roomItem;
    GazeTimedInput input;

    // Use this for initialization
	void Start () {
        roomItem = GetComponentInParent<RoomItem>();
        input = GetComponent<GazeTimedInput>();
        if (input != null)
            input.OnGazeSelect += roomItem.OnJoinButton;
	}
}

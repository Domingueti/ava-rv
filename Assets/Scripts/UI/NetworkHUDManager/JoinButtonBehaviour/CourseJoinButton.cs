﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class CourseJoinButton : MonoBehaviour, IJoinButton {

    public Course Course { get; set; }

	public void OnJoinButton (MatchInfoSnapshot room, string roomName) {
        if (room == null) {
            Course.IsActive = true;
            //NetManager.CurrentCourse = Course;
            //NetManager.CurrentCourse.IsActive = true;
            NetworkHUD.singleton.OnClickCreateCourse(roomName, true);
        } else {
            NetworkHUD.singleton.OnClickJoinMatch(room, "");
        }
    }
}


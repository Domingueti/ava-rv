﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;

public class CourseListContent : MonoBehaviour, IListContent {

	public string Filter { get; set; }

	public void ShowOnline () {
        NetworkHUD.singleton.OnClickListMatches (Filter);
	}

	public void ShowOffline (GameObject contentPanel) {
        List<Course> list = NetManager.netSingleton.CourseList;
		foreach (Course cr in list) {
			if (!cr.IsActive) {
				GameObject item = (GameObject)Instantiate (Resources.Load("RoomItemList"));
				item.GetComponent<RoomItem> ().Setup (cr);
                item.GetComponent<RoomItem>().SetCourseButtonAction();
				item.transform.SetParent (contentPanel.transform);
                item.SetActive(true);
			}
		}
	}
}


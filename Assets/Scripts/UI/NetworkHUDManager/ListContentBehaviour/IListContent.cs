﻿using System;
using UnityEngine;
using System.Collections.Generic;

public interface IListContent {
	string Filter { get; set; }

	void ShowOnline ();
	void ShowOffline(GameObject contentPanel);
}



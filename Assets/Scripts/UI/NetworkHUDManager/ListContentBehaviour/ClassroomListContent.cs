﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class ClassroomListContent :  MonoBehaviour, IListContent {

	public string Filter { get; set; }
    public GameObject CreateClassroomButton;

	public void ShowOnline () {
        NetworkHUD.singleton.OnClickListMatches (Filter);
	}

	public void ShowOffline (GameObject contentPanel) {
        GameObject item = Instantiate (Resources.Load("RoomItemWorld")) as GameObject;
        item.GetComponent<RoomItem> ().Setup ("Sala Privada");
        item.GetComponent<RoomItem>().SetClassroomButtonAction();
        item.transform.SetParent (contentPanel.transform, false);
		item.SetActive (true);
        
	}
    void OnClickCreateNew() {
        
    }
}


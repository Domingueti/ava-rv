﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ListView : MonoBehaviour {

    public static GameObject contentPanel;
	public GameObject RoomItemObject;
    public GameObject createClassroomButton;
    private IListContent listContentManager;
    public Text titleText;

    void Awake() {
        contentPanel = GetComponentInChildren<VerticalLayoutGroup>().gameObject;
    }

    private void Start() {
        SetClassContent();
        gameObject.SetActive(false);

    }

    public void ShowList() {
        DestroyListContent();
		ShowOnline ();
		ShowOffline ();
	}

	void ShowOnline() {
		if (listContentManager != null)
			listContentManager.ShowOnline ();
	}

	void ShowOffline() {
		if (listContentManager != null)
			listContentManager.ShowOffline (contentPanel);
	}

	public void SetCourseContent() {
		//Debug.Log("Set Course Content");
        if (!(listContentManager == typeof (CourseListContent))) {
            DestroyContentManager();
            listContentManager = gameObject.AddComponent<CourseListContent>();
            listContentManager.Filter = "";
            titleText.text = "Lista de Disciplinas";
        }

        ShowList();
	}

	public void SetClassContent() {
        //Debug.Log("Set Class Content");
        if (!(listContentManager == typeof(ClassroomListContent))) {
            DestroyContentManager();
            createClassroomButton.SetActive(true);
            listContentManager = gameObject.AddComponent<ClassroomListContent>();
            titleText.text = "Salas disponíveis";
        }
        ShowList();
    }

	void DestroyContentManager() {
		if (listContentManager != null) {
            DestroyListContent();
            var obj = GetComponent<IListContent>() as Component;
            createClassroomButton.SetActive(false);
			Destroy (obj);
        }
	}

	void DestroyListContent() {
        RoomItem[] childs = contentPanel.GetComponentsInChildren<RoomItem>();
        for (int i=0; i<childs.Length; i++) {
            if(childs[i].gameObject != null)
                Destroy(childs[i].gameObject);
        }
	}

}


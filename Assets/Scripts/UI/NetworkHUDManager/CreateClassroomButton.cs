﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateClassroomButton : MonoBehaviour {
    GazeTimedInput input;

	// Use this for initialization
	void Start () {
        input = GetComponent<GazeTimedInput>();
        if (input != null)
            input.OnGazeSelect += OnClick;
	}
	
	public void OnClick() {
        NetworkHUD.singleton.OnClickCreateClassroom(RoomManager.GetClassroomName(string.Empty, NetManager.netSingleton.CurrentUser.Name), true, "Classroom");
    }
}

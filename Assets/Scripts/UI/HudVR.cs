﻿using UnityEngine;

public class HudVR : MonoBehaviour {
    //Makes the VR HUD follow the player
    public GameObject hud;
    public Vector3 offset;

    public void Update()
    {
        hud.transform.position = transform.position + offset;
    }

}

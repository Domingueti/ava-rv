﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SimpleMessageBox : MonoBehaviour {

    public GameObject contentObject;
    public Text textLabel;

    public static SimpleMessageBox singleton;

    private void Start() {
        if (singleton != null) {
            Destroy(this);
        } else {
            singleton = this;
        }
    }

    public void DisplayText(string textString) {
        StartCoroutine("ShowMessage", textString);
    }

    private IEnumerator ShowMessage(string text) {
        textLabel.text = text;
        contentObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        contentObject.SetActive(false);
    }

}

﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;


public class ByteOperations {
    public static byte[] ObjectToByteArray(Object obj) {
        if (obj == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream()) {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    public static Object ByteArrayToObject(byte[] data) {

        MemoryStream ms = new MemoryStream();
        BinaryFormatter binFormater = new BinaryFormatter();
        ms.Write(data, 0, data.Length);
        ms.Seek(0, SeekOrigin.Begin);

        Object obj = (Object)binFormater.Deserialize(ms);
        return obj;
    }

    public static byte[] CompressData(byte[] data) {
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(output, CompressionLevel.Optimal))
        {
            dstream.Write(data, 0, data.Length);
        }
        return output.ToArray();
    }

    public static byte[] DecompressData(byte[] data) {
        MemoryStream input = new MemoryStream(data);
        MemoryStream output = new MemoryStream();
        byte[] buffer = new byte[1024];
        using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress)) {
            int count = 0;
            do {
                count = dstream.Read(buffer, 0, 1024);
                output.Write(buffer, 0, count);
            } while (count > 0);
        }
        return output.ToArray();
    }
}

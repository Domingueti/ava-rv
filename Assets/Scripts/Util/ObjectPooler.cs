﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {
	
	/* Generic Object Pooler Class */

	private List<GameObject> objectList;
	public GameObject prefab;

	public int amount = 3;
	public bool willGrow = true;

	void Start () {
		objectList = new List<GameObject> ();
		for (int i = 0; i < amount; i++) {
			GameObject obj = (GameObject)Instantiate (prefab);
			obj.SetActive (false);
			objectList.Add (obj);
		}
	}

	/* Search for an usable object. If there's none, create a new one and add it on the List */
	public GameObject GetObject() {
		for (int i = 0; i < objectList.Count; i++) {
			if (!objectList [i].activeInHierarchy) {
				return objectList [i];
			}
		}

		if (willGrow == true) {
			GameObject obj = (GameObject)Instantiate (prefab);
			objectList.Add (obj);
			return obj;
		}
		return null;
	}
		

	public List<GameObject> ObjectList {
		get { return this.objectList; }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SceneManagement;
using UnityEngine.Networking.Match;
using UnityEngine.Networking;
public class SwitchVrMode : MonoBehaviour {

    IEnumerator LoadDevice() {
        XRSettings.LoadDeviceByName("cardboard");
        yield return null;
        XRSettings.enabled = true;
    }

    public void LoadVrScene(string scene) {
        StartCoroutine("SetVrMode", scene);
    }

    public void LoadOnlineVrScene(MatchInfo matchInfo) {
        StartCoroutine("SetOnlineVr", matchInfo);
    }

    private IEnumerator SetOnlineVr(MatchInfo matchInfo) {
        XRSettings.LoadDeviceByName("cardboard");
        yield return null;
        if (!string.IsNullOrEmpty(XRSettings.loadedDeviceName)) {
            Debug.Log("Created Match Id " + matchInfo.networkId);
            MatchInfo host = matchInfo;
            NetworkServer.Listen(host, 9000);
            NetworkServer.Reset();
            NetManager.netSingleton.StartHost(host);
        }
    }

    private IEnumerator SetVrMode(string scene) {
        XRSettings.LoadDeviceByName("cardboard");
        yield return null;
        if (XRSettings.loadedDeviceName.Equals("cardboard")) {
            SimpleMessageBox.singleton.DisplayText(XRSettings.loadedDeviceName);
            XRSettings.enabled = true;
            SceneManager.LoadScene(scene);
        } else {
            SimpleMessageBox.singleton.DisplayText("Dispositivo não compatível!");
            XRSettings.enabled = false;
            if (NetManager.netSingleton != null)
                NetManager.netSingleton.SetTeacher();
        }
    }

}

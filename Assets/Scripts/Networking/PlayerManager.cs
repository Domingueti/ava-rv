﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour {

    [SerializeField] private GameObject[] onlinePlayers;
    [SerializeField] private GameObject teacher;

    //Sync the player names over all online players
    [Server]
    public void SyncPlayersName() {
        onlinePlayers = GameObject.FindGameObjectsWithTag("Player");
        teacher = GameObject.FindGameObjectWithTag("PlayerTeacher");
        foreach (GameObject go in onlinePlayers) {
            if (gameObject.GetComponent<SetupPlayerHUD>() != null)
                gameObject.GetComponent<SetupPlayerHUD>().RpcSetupPlayerName();
        }
        if (teacher != null)
            teacher.GetComponent<SetupPlayerHUD>().RpcSetupPlayerName();
    }

}

﻿using System;

[Serializable]
public class ContentTextureMsg : ContentBaseMsg {
    public byte[] Texture { get; set; }
    public bool isEditing;
    
    public ContentTextureMsg(byte[] tex, bool isEditing) {
        this.Texture = tex;
        this.isEditing = isEditing;
    }

    public override string ToString() {
        return base.ToString() + " Tex Size: " + Texture.Length + " IsEditing: " + isEditing;
    }

}


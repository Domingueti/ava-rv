﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ContentVideoMsg : ContentBaseMsg {

    public long CurrentFrame;
    public string Url;
    public bool isPlaying;

    public ContentVideoMsg(long frame, string url, bool isPlaying) {
        CurrentFrame = frame;
        Url = url;
        this.isPlaying = isPlaying;
    }

    public override string ToString() {
        return "Frame: " + CurrentFrame + " Url: " + Url + " Paused: " + isPlaying;
    }

}

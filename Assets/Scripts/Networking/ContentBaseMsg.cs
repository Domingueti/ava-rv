﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ContentBaseMsg {
    public uint type;

    public override string ToString() {
        return "Type: " + type;
    }
}

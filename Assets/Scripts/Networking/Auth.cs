﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class Auth : MonoBehaviour {

    public Dropdown selectClient;
    public InputField nameInput;
    SwitchVrMode switchVr;

    private void Start() {
        XRSettings.enabled = false;
        nameInput.text = PlayerPrefs.GetString("Name", string.Empty);
        switchVr = GetComponent<SwitchVrMode>();
        PlayerPrefs.SetInt("IsStudent", 0);
        PlayerPrefs.SetInt("IsTeacher", 0);
    }

    public void OnLogin() {
        string choose = selectClient.captionText.text;
        string playerName = nameInput.text;
        if (string.IsNullOrEmpty(playerName)) {
            SimpleMessageBox.singleton.DisplayText("O campo 'Nome' não pode ser vazio");
        } else {
            PlayerPrefs.SetString("Name", playerName);
            if (choose.Equals("Estudante")) {
                PlayerPrefs.SetInt("IsStudent", 1);
                PlayerPrefs.SetInt("IsTeacher", 0);
                //SceneManager.LoadScene("MainLobby");
               switchVr.LoadVrScene("MainLobby");
            } else {
                PlayerPrefs.SetInt("IsStudent", 0);
                PlayerPrefs.SetInt("IsTeacher", 1);
                SceneManager.LoadScene("MainInstructor");
            }
        }
	}
}

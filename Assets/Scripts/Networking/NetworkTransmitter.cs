﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using System;

/* Baseado em: https://answers.unity.com/questions/1113376/unet-send-big-amount-of-data-over-network-how-to-s.html
 * Acesso em 31/08/2018
 * 
 */
#region old
/*

 public class NetworkTransmitter : NetworkBehaviour { 

private class TransmisionData : IComparable<TransmisionData> {
    public int currentIndex;
    public byte[] data;

    public TransmisionData(int index, byte[] data) {
        this.currentIndex = index;
        this.data = data;
    }

    public int CompareTo(TransmisionData other) {
        if (currentIndex < other.currentIndex)
            return -1;
        else
            return 1;
    }
}

List<TransmisionData> dataTransferList;

public int BufferSize = 1024;
private byte[] dataReceived;
private int dataLenght;
private int amountReceived;
private int expectedChunks = 0;
private bool isTransmitting;

public event UnityAction<int, byte[]> OnChunkReceived;
public event UnityAction<byte[]> OnDataCompletelyReceived;

[Server]
public void SendDataToClients(byte[] data) {
    Debug.LogWarning("Size: " + BufferSize);
    StartCoroutine("SendBytes", data);
}

[Server]
public IEnumerator SendBytes(byte[] data) {
    int currByte = 0;
    byte[] dataChunk = null;
    int currIndex = 0;
    int lenght = BufferSize;
    RpcPrepareDataTransfer(data.Length);
    yield return null;

    while (currByte < data.Length) {
        lenght = data.Length - currByte;

        if (lenght > BufferSize)
            lenght = BufferSize;

        dataChunk = new byte[lenght];

        Array.Copy(data, currByte, dataChunk, 0, lenght);

        Debug.Log("Chunk " + currIndex + " sended.");
        RpcReceiveChunk(currIndex, currByte, dataChunk);
        currByte += BufferSize;
        currIndex++;

        yield return new WaitForEndOfFrame();
    }
}

[ClientRpc]
void RpcPrepareDataTransfer(int expectedSize) {
    dataLenght = expectedSize;
    dataReceived = new byte[expectedSize];
    expectedChunks = expectedSize % BufferSize + 1;
    Debug.LogWarning("Expected: " + expectedSize);
    dataTransferList = new List<TransmisionData>();
    Debug.LogWarning("Rpc Prepared");
}


[ClientRpc]
void RpcReceiveChunk(int id, int index, byte[] data) {
    LogManager.singleton.DisplayLog("Chunk " + id + " received. ");
    if (dataTransferList == null)
        dataTransferList = new List<TransmisionData>();

    amountReceived += data.Length;
    TransmisionData newData = new TransmisionData(index, data);
    dataTransferList.Add(newData);

    if (amountReceived == dataLenght) {
        Debug.LogWarning("Terminou");
        LogManager.singleton.DisplayLog("Terminou!!!");
        dataTransferList.Sort();
        foreach (TransmisionData item in dataTransferList) {
            Debug.Log("Lenght: " + item.data.Length + " Index: " + item.currentIndex);
            Array.Copy(item.data, 0, dataReceived, item.currentIndex, item.data.Length);
        }


        Debug.LogWarning("Rpc Recv Finished");
        if (OnDataCompletelyReceived != null)
            OnDataCompletelyReceived(dataReceived);
    }

    if (OnChunkReceived != null)
        OnChunkReceived(id, data);
}

}
 */

#endregion
public class NetworkTransmitter : NetworkBehaviour
{

    private static readonly string LOG_PREFIX = "[" + typeof(NetworkTransmitter).Name + "]: ";
    public const int RELIABLE_SEQUENCED_CHANNEL = 0;
    private static int defaultBufferSize = 1024; //max ethernet MTU is ~1400

    private class TransmissionData
    {
        public int curDataIndex; //current position in the array of data already received.
        public byte[] data;

        public TransmissionData(byte[] _data)
        {
            curDataIndex = 0;
            data = _data;
        }
    }

    //list of transmissions currently going on. a transmission id is used to uniquely identify to which transmission a received byte[] belongs to.
    List<int> serverTransmissionIds = new List<int>();

    //maps the transmission id to the data being received.
    Dictionary<int, TransmissionData> clientTransmissionData = new Dictionary<int, TransmissionData>();

    //callbacks which are invoked on the respective events. int = transmissionId. byte[] = data sent or received.
    public event UnityAction<int, byte[]> OnDataComepletelySent;
    public event UnityAction<int, byte[]> OnDataFragmentSent;
    public event UnityAction<int, byte[]> OnDataFragmentReceived;
    public event UnityAction<int, byte[]> OnDataCompletelyReceived;

    [Server]
    public void SendBytesToClients(int transmissionId, byte[] data)
    {
        Debug.Assert(!serverTransmissionIds.Contains(transmissionId));
        StartCoroutine(SendBytesToClientsRoutine(transmissionId, data));
    }

    [Server]
    public IEnumerator SendBytesToClientsRoutine(int transmissionId, byte[] data)
    {
        Debug.Assert(!serverTransmissionIds.Contains(transmissionId));
        Debug.LogWarning(LOG_PREFIX + "SendBytesToClients processId=" + transmissionId + " | datasize=" + data.Length);

        //tell client that he is going to receive some data and tell him how much it will be.
        RpcPrepareToReceiveBytes(transmissionId, data.Length);
        yield return null;

        //begin transmission of data. send chunks of 'bufferSize' until completely transmitted.
        serverTransmissionIds.Add(transmissionId);
        TransmissionData dataToTransmit = new TransmissionData(data);
        int bufferSize = defaultBufferSize;
        while (dataToTransmit.curDataIndex < dataToTransmit.data.Length - 1)
        {
            //determine the remaining amount of bytes, still need to be sent.
            int remaining = dataToTransmit.data.Length - dataToTransmit.curDataIndex;
            if (remaining < bufferSize)
                bufferSize = remaining;

            //prepare the chunk of data which will be sent in this iteration
            byte[] buffer = new byte[bufferSize];
            System.Array.Copy(dataToTransmit.data, dataToTransmit.curDataIndex, buffer, 0, bufferSize);

            //send the chunk
            RpcReceiveBytes(transmissionId, buffer);
            dataToTransmit.curDataIndex += bufferSize;

            yield return null;

            if (null != OnDataFragmentSent)
                OnDataFragmentSent.Invoke(transmissionId, buffer);
        }

        //transmission complete.
        serverTransmissionIds.Remove(transmissionId);

        if (null != OnDataComepletelySent)
            OnDataComepletelySent.Invoke(transmissionId, dataToTransmit.data);
    }

    [ClientRpc]
    private void RpcPrepareToReceiveBytes(int transmissionId, int expectedSize)
    {
        if (clientTransmissionData.ContainsKey(transmissionId))
            return;
        //prepare data array which will be filled chunk by chunk by the received data
        TransmissionData receivingData = new TransmissionData(new byte[expectedSize]);
        clientTransmissionData.Add(transmissionId, receivingData);
    }

    //use reliable sequenced channel to ensure bytes are sent in correct order
    [ClientRpc(channel = RELIABLE_SEQUENCED_CHANNEL)]
    private void RpcReceiveBytes(int transmissionId, byte[] recBuffer)
    {
        //already completely received or not prepared?
        if (!clientTransmissionData.ContainsKey(transmissionId))
            return;

        //copy received data into prepared array and remember current dataposition
        TransmissionData dataToReceive = clientTransmissionData[transmissionId];
        System.Array.Copy(recBuffer, 0, dataToReceive.data, dataToReceive.curDataIndex, recBuffer.Length);
        dataToReceive.curDataIndex += recBuffer.Length;

        if (null != OnDataFragmentReceived)
            OnDataFragmentReceived(transmissionId, recBuffer);

        if (dataToReceive.curDataIndex < dataToReceive.data.Length - 1)
            //current data not completely received
            return;

        //current data completely received
        Debug.LogWarning(LOG_PREFIX + "Completely Received Data at transmissionId=" + transmissionId);
        clientTransmissionData.Remove(transmissionId);

        if (null != OnDataCompletelyReceived)
            OnDataCompletelyReceived.Invoke(transmissionId, dataToReceive.data);
    }
}

﻿using System.Collections;
using UnityEngine.Networking;
using System;

[Serializable]
public class ContentSlidesMsg : ContentBaseMsg {

    public int Index { get; set; }
    public byte[] Texture { get; set; }

    public ContentSlidesMsg(int index, byte[] texture) {
        Index = index;
        Texture = texture;
    }

    public ContentSlidesMsg() {
        Index = -1;
        Texture = null;
    }

    public override string ToString() {
        return "Index: " + Index + "Size: " + Texture.Length;
    }

}

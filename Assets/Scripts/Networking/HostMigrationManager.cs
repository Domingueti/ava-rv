﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class HostMigrationManager :  NetworkMigrationManager {

	MatchInfoSnapshot info;

	protected override void OnClientDisconnectedFromHost (NetworkConnection conn, out SceneChangeOption sceneChange) {
		base.OnClientDisconnectedFromHost (conn, out sceneChange);
		if (BecomeNewHost (NetworkManager.singleton.networkPort)) {
			Debug.Log ("Success!");
		} else {
			Debug.Log ("Could not do this!");
		}
	}



}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;
using UnityEngine.UI;
public class SetupPlayerHUD :  NetworkBehaviour {

    public MicButton micButton;
    public GameObject boardPrefab;
    private GameObject teacherHuDInstance;
    private GameObject hudClassroom;
    public GameObject boardRespawn;
    public GameObject boardManager;
    public GameObject playerManager;
    public GameObject playerNameCanvas;
    public GameObject vrCamera;
    public GameObject vrHUD;
    public GameObject vrHUDClassroom;
    public GameObject teacherCamera;
    public GameObject visor;

    public override void OnStartLocalPlayer() {
       // Camera.main.gameObject.SetActive(false);

        hudClassroom = GameObject.FindGameObjectWithTag("HUDClassroom");
        teacherHuDInstance = GameObject.FindGameObjectWithTag("TeacherHud");
        GameObject mainHud = GameObject.FindGameObjectWithTag("MainHud");
        Debug.Log("Main Hud ===> ");
        Debug.Log(mainHud == null);
        if (NetManager.netSingleton.IsTeacher) {
            Debug.Log("Professor");
            micButton = mainHud.GetComponentInChildren<MicButton>();
            teacherHuDInstance.SetActive(true);
            GetComponent<PlayerControllerOnline>().mouseLookEnabled = false;
            GameObject[] gvrObjects = GameObject.FindGameObjectsWithTag("GvrItem");
            for (int i = 0; i < gvrObjects.Length; i++)
                gvrObjects[i].SetActive(false);
            teacherCamera.SetActive(true);
            playerNameCanvas.transform.SetParent(teacherCamera.transform);
        } else {
            //micButton = vrHUD.GetComponentInChildren<MicButton>();
            //micButton.OpusClient = GetComponent<OpusNetworked>();
            //micButton.OpusClient.IsRecording = false;
            //micButton.SetOnlineMic();
            Debug.Log("Estudante");
            teacherHuDInstance.SetActive(false);
            mainHud.SetActive(false);
            hudClassroom.SetActive(false);
            vrHUD.SetActive(true);
            vrCamera.SetActive(true);
            Debug.Log("VR Camera: " + vrCamera.active);
            Debug.Log("VR Camera: " + vrCamera.activeInHierarchy);

            FloorMovement floor = GameObject.Find("ClassRoom").GetComponentInChildren<FloorMovement>();
            floor.SetOnlinePlayerMovement(GetComponent<PlayerControllerOnline>());

            //visor.transform.position += new Vector3(0, 0.58f, 0);
            visor.transform.SetParent(vrCamera.transform);
            //playerNameCanvas.transform.position += new Vector3(0, 0.58f, 0);
            playerNameCanvas.transform.SetParent(vrCamera.transform);
            Debug.Log("Setup Complete");
        }

        if (!isServer) {
            hudClassroom.SetActive(false);
            NetManager.netSingleton.boardManager = GameObject.FindGameObjectWithTag("BoardManager").GetComponent<BoardManagement>();
            NetManager.netSingleton.playerManager = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<PlayerManager>();
        } else {
           if (NetManager.netSingleton.IsStudent)
                vrHUDClassroom.SetActive(true);
            CmdInstantiateBoard();    
        }

        CmdSetPlayerName(PlayerPrefs.GetString("Name"));
        Debug.Log("Set Player Name Setup Full Completed");
    }

    

    //receive a msg from server to sync current name with others players
    [ClientRpc]
    public void RpcSetupPlayerName() {
        if (string.IsNullOrEmpty(playerNameCanvas.GetComponentInChildren<Text>().text))
            CmdSetPlayerName(PlayerPrefs.GetString("name"));
    }

    [Command]
    void CmdSetPlayerName(string name) {
        OnServerSetPlayerName(name);
    }

    [Server]
    void OnServerSetPlayerName(string name) {
        RpcSetPlayerName(name);
    }

    [ClientRpc]
    void RpcSetPlayerName(string name) {
        Debug.Log("Received => " + name);
        Text canvasName = GetComponentInChildren<Text>();
        //if (string.IsNullOrEmpty(canvasName.text))
            canvasName.text = name;
    }

    [Command]
    void CmdInstantiateBoard() {
        //create the camera container object
        GameObject cameraList = new GameObject();
        cameraList.name = "BoardsCameras";
        cameraList.transform.position = new Vector3(0f, 0f, 0f);
        //instantiate the boards
        GameObject[] boardPositions = GameObject.FindGameObjectsWithTag("BoardSpawn");
        GameObject previousButton = null;
        GameObject playButton = null;
        GameObject nextButton = null;
        
        if (NetManager.netSingleton.IsTeacher) {
            foreach (Transform t in hudClassroom.GetComponentInChildren<Transform>()) {
                if (t.gameObject.name.Equals("Button_Previous")) {
                    previousButton = t.gameObject;
                }

                if (t.gameObject.name.Equals("Button_PlayPause")) {
                    playButton = t.gameObject;
                }

                if (t.gameObject.name.Equals("Button_Next")) {
                    nextButton = t.gameObject;
                }
            }
        } else {
            foreach (Transform t in vrHUDClassroom.GetComponentInChildren<Transform>()) {
                if (t.gameObject.name.Equals("Button_Previous")) {
                    previousButton = t.gameObject;
                }

                if (t.gameObject.name.Equals("Button_PlayPause")) {
                    playButton = t.gameObject;
                }

                if (t.gameObject.name.Equals("Button_Next")) {
                    nextButton = t.gameObject;
                }
            }
        }

        for (int i = 0; i < boardPositions.Length; i++) {
            GameObject boardInstance = Instantiate(boardPrefab, boardPositions[i].transform.position, boardPositions[i].transform.rotation);
	        float scaleX = boardInstance.transform.localScale.x * boardPositions[i].transform.localScale.x;
			float scaleY = boardInstance.transform.localScale.y * boardPositions[i].transform.localScale.y;
			float scaleZ = boardInstance.transform.localScale.z * boardPositions[i].transform.localScale.z;
			boardInstance.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
            boardInstance.name = "Board " + (i + 1).ToString();
			if (NetManager.netSingleton.IsTeacher) {
				GameObject camObj = new GameObject();
				camObj.name = "Camera Board " + i;
				camObj.AddComponent<Camera>();
				camObj.transform.SetParent(cameraList.transform);
				camObj.transform.position = new Vector3(0, 3.6f, 0);
				camObj.transform.LookAt(boardInstance.transform.position);
			}
			NetworkServer.Spawn(boardInstance);
            boardInstance.GetComponent<ContentView>().previousButton = previousButton;
            boardInstance.GetComponent<ContentView>().playButton = playButton;
            boardInstance.GetComponent<ContentView>().nextButton = nextButton;
        }
        //create board manager and setup cameras
        GameObject boardMan = Instantiate(boardManager);
        GameObject playerMan = Instantiate(playerManager);
        NetworkServer.Spawn(boardMan);
        NetworkServer.Spawn(playerMan);
        NetManager.netSingleton.boardManager = boardMan.GetComponent<BoardManagement>();
        NetManager.netSingleton.playerManager = playerMan.GetComponent<PlayerManager>();
        boardMan.GetComponent<BoardManagement>().RpcSetupBoards(0);
        if (NetManager.netSingleton.IsTeacher)
            teacherHuDInstance.GetComponentInChildren<SwitchCamera>().SetupCameraList(cameraList);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;
using UnityEngine.Networking.NetworkSystem;

public class NetManager : NetworkManager {
    public User CurrentUser { get; set; }
    public List<Course> CourseList { get; set; }

    public MatchInfoSnapshot CurrentMatch;
    public static NetManager netSingleton;
    public bool IsStudent { get; set; }
    public bool IsTeacher { get; set; }
    public List<string> onlineUsers;

    //boards management
    public BoardManagement boardManager;

    //spawn management
    NetworkStartPosition teacherPosition;
    GameObject[] studentPositions;
    int studentLastUsed = -1;

    //player management
    public PlayerManager playerManager;

    public class PlayerTypeMsg : MessageBase {
        public int type;
    }

    private void Awake() {
        CurrentUser = new User(PlayerPrefs.GetString("Name"), string.Empty, PlayerPrefs.GetString("Name"));
        int student = PlayerPrefs.GetInt("IsStudent");
        int teacher = PlayerPrefs.GetInt("IsTeacher");
        PlayerPrefs.SetInt("IsStudent", 0);
        PlayerPrefs.SetInt("IsTeacher", 0);
        if (student > 0)
            IsStudent = true;
        else
            IsStudent = false;

        if (teacher > 0)
            IsTeacher = true;
        else
            IsTeacher = false;

        if (netSingleton != null)
            Destroy(this);
        else
            netSingleton = this;

        if (IsStudent)
            offlineScene = "MainLobby";
        else
            offlineScene = "MainInstructor";

        StartMatchMaker();
        CurrentUser = new User(PlayerPrefs.GetString("Name"), string.Empty, PlayerPrefs.GetString("Name"));
        CourseList = new List<Course>();
        RoomManager.SetupList(CourseList);
    }


    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader) {
        Transform currentPosition = null;
        PlayerTypeMsg msg = extraMessageReader.ReadMessage<PlayerTypeMsg>();

        if (teacherPosition == null)
            teacherPosition = GameObject.FindGameObjectWithTag("TeacherSpawn").GetComponent<NetworkStartPosition>();
        if (studentPositions == null)
            studentPositions = GameObject.FindGameObjectsWithTag("StudentSpawn");
            

        if (msg.type == 0) // is student
            currentPosition = GetStudentPosition();
        else // is teacher
            currentPosition = teacherPosition.transform;

        GameObject player = (GameObject)Instantiate(playerPrefab, currentPosition.transform.position, currentPosition.transform.rotation);
        if (msg.type == 1)
            player.tag = "PlayerTeacher";

        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        if (playerManager != null) {
            playerManager.SyncPlayersName();
        }
        boardManager.RpcSetupBoards(boardManager.CurrentBoard);
        boardManager.UpdateBoardsContent();
    }

    public override void OnServerSceneChanged(string sceneName) {
        if (sceneName == "MainLobby" || sceneName == "MainInstructor") {
            StopHost();
            StopMatchMaker();
            StartMatchMaker();
        }
        studentLastUsed = 0;
    }

    public override void OnClientConnect(NetworkConnection conn) {
        conn.SetChannelOption(0, ChannelOption.MaxPendingBuffers, 64);
        PlayerTypeMsg message = new PlayerTypeMsg();
        if (IsStudent)
            message.type = 0;
        else
            message.type = 1;

        ClientScene.AddPlayer(conn, 0, message);
    }

    public override void OnClientSceneChanged(NetworkConnection conn) {
        //base.OnClientSceneChanged(conn);
    }

    public override void OnSetMatchAttributes(bool success, string extendedInfo) {
        base.OnSetMatchAttributes(success, extendedInfo);
        if (!success) {
            Debug.Log("Unable to update the match information " + extendedInfo);
            SimpleMessageBox.singleton.DisplayText("Não foi possível atualizar as informações da sala.");
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn) {
        base.OnServerDisconnect(conn);
        CurrentMatch = null;
    }

    public override void OnClientDisconnect(NetworkConnection conn) { 
        base.OnClientDisconnect(conn);
        Debug.Log("Player Left");
    }

    public override void OnClientError(NetworkConnection conn, int errorCode) {
        base.OnClientError(conn, errorCode);
        SimpleMessageBox.singleton.DisplayText("Houve um erro inesperado. Código: " + errorCode);
        StopHost();
        StartMatchMaker();
    }

    public void DisconnectClient() {
        //if (NetManager.netSingleton.CurrentMatch.currentSize == 1) {
        matchMaker.SetMatchAttributes(NetManager.netSingleton.matchInfo.networkId, false, 0, NetManager.netSingleton.OnSetMatchAttributes);
        //}
        studentLastUsed = 0;
        StopHost();
        StartMatchMaker();
    }

    Transform GetStudentPosition() {
        studentLastUsed++;
        if (studentLastUsed > studentPositions.Length - 1)
            studentLastUsed = 0;

        return studentPositions[studentLastUsed].transform;
    }

    public void SetStudent() {
        this.IsStudent = true;
        this.IsTeacher = false;
    }
    
    public void SetTeacher() {
        this.IsStudent = false;
        this.IsTeacher = true;
    }

}
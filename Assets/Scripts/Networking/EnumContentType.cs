﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumContentType {
    public enum ContentType { NONE, TEXTURE, SLIDE, VIDEO};
}

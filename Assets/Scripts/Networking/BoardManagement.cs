﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BoardManagement : NetworkBehaviour {
    
    [SerializeField] GameObject[] boardInstances;
    int currentBoard;

    public int Count {
        get { return this.boardInstances.Length; }
    }

    public int CurrentBoard {
        get { return this.currentBoard; }
        set { this.currentBoard = value; }
    }

    [Server]
    public void OnBoardChange(int value) {
        boardInstances[currentBoard].GetComponent<ContentView>().StoreStatus();
        currentBoard = value;
        boardInstances[currentBoard].GetComponent<ContentView>().ResetButtonsStatus();
        boardInstances[currentBoard].GetComponent<ContentView>().SetButtonsActions();
        RpcDistributeCurrentBoard(currentBoard);
    }

    [ClientRpc]
    public void RpcSetupBoards(int current) {
        boardInstances = GameObject.FindGameObjectsWithTag("BoardClassroom");
        Debug.Log(boardInstances.Length);
        currentBoard = current;
        Debug.Log(currentBoard);
    }

    [Command]
    void CmdGetBoardContent() {
        UpdateBoardsContent();
    }

    [Server]
    public void UpdateBoardsContent() {
        Debug.Log("Rpc Update Boards");
        if (boardInstances != null)
            for (int i = 0; i < boardInstances.Length; i++) {
                if (boardInstances[i].GetComponent<ContentView>().Type != (uint)EnumContentType.ContentType.NONE) {
                    boardInstances[i].GetComponent<ContentView>().StartCoroutine("SendData");
                }
            }
    }

    [Server]
    public void UpdateCurrentBoard() {
        boardInstances[currentBoard].GetComponent<ContentView>().StartCoroutine("SendData");
    }

    [ClientRpc]
    public void RpcDistributeCurrentBoard(int value) {
        currentBoard = value;
    }

    public GameObject GetActiveBoard() {
        Debug.Log("Length: " + boardInstances.Length + " - CB: " + CurrentBoard);
        if (boardInstances != null)
            return boardInstances[currentBoard];
        return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FloorMovementOnline : NetworkBehaviour {
    
    public GazeTimedInput GazeInput { get; }

    public void SetPlayerMovement(PlayerControllerOnline player) {
        GazeInput.OnGazeSelect += player.MoveForward;

    }

}

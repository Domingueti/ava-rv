﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportaTeste : MonoBehaviour {

    private void Start() {
       GetComponent<GazeTimedInput>().OnGazeSelect += Teleporte;
    }

    void Teleporte() {
        SceneManager.LoadScene("Confessional");        
    }
}

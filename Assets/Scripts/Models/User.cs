﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User {

	private string login;
	private string password;
	private string name;
	private bool status;


	public User(string login, string password, string name) {
		this.login = login;
		this.password = password;
		this.name = name;
		this.status = false;
	}

	public string Login {
		get { return this.login; }
		set { this.login = value; }
	}

	public string Password {
		get { return this.password; }
	}

	public string Name {
		get { return this.name; }
		set { this.name = value; }
	}

	public bool Status {
		get { return this.status; }
		set { this.status = value; }
	}
}

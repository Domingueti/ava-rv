﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class Message {
	public string Title { get; set; }
	public string Content { get; set; }
	public string Sender { get; set; }
	public DateTime SendTime { get; set; }

	public Message(string title, string content, string sender, DateTime sendTime) {
		this.Title = title;
		this.Content = content;
		this.Sender = sender;
		this.SendTime = sendTime;
	}

	public override string ToString () {
		return "Title: " + Title + " Sender: " + Sender + " SendTime: " + SendTime.ToString () + " Content: " + Content;
	}

}

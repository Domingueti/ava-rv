﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Course {

	public string Name { get; set; }
	public bool IsActive { get; set; }

	public Course(string name) {
		this.Name = name;
		this.IsActive = false;
	}

	public override string ToString () {
		return string.Format ("[Course: IsActive={0}, Name={1}]", IsActive, Name);
	}
}

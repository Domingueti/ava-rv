﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimedTest : MonoBehaviour {

    private GazeTimedInput input;
    public Text text;    
    private void Start()
    {
        input = GetComponent<GazeTimedInput>();
        input.OnGazeSelect += Teste;
    }

    public void Teste() {
        text.text = "Clicou com o olho finalmente";
    }

}
